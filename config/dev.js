const dev = module.exports;

dev.info = {
  'id' : 'dev',
  'api_server_host' : 'devapi.nemodax.com',
  'api_server_port' : '443',
  'erc20_sc_address' : '0x6450de6fb05367a59e9cead5abe66c9bcb80a20a',
  'point_charge_address' : '0x6450de6fb05367a59e9cead5abe66c9bcb80a20a',
  'etherscan_web_uri' : 'https://rinkeby.etherscan.io',
  's3_bucket' : 'nemodax-upload-dev',
  's3_download_url' : 'https://nemodax-dev.s3.ap-northeast-2.amazonaws.com'
}
