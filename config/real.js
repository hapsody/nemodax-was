const real = module.exports;

real.info = {
  'id' : 'real',
  'api_server_host' : 'api.nemodax.com',
  'api_server_port' : '443',
  'erc20_sc_address' : '0x957b28f93B0e01557E21e6c564Ab26ddc2d18EC5',
  'point_charge_address' : '0x957b28f93B0e01557E21e6c564Ab26ddc2d18EC5',
  'etherscan_web_uri' : 'https://etherscan.io',
  's3_bucket' : 'nemodax-upload',
  's3_download_url' : 'https://nemodax.s3.ap-northeast-2.amazonaws.com'
}
