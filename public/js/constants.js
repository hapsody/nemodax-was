
const OS_CD = 'WEB';

const FILE_SEARCH_TYPE = {
  "LATEST":"LATEST",
  "POPULAR":"POPULAR",
  "FAVORITE":"FAVORITE"
};

//
// const CATEGORY = {
//   // 4 : {
//   //   category_no : 4,
//   //   category_name : 'Entertain',
//   //   depth : 1,
//   //   order_num : 4,
//   //   sub_category :[
//   //     {
//   //       category_no : 15,
//   //       category_name : '비디오',
//   //       depth : 2,
//   //       order_num : 1
//   //     },
//   //     {
//   //       category_no : 16,
//   //       category_name : '음악',
//   //       depth : 2,
//   //       order_num : 1
//   //     }
//   //   ]
//   // },
//   5 : {
//     category_no : 5,
//     category_name : 'STUDY',
//     depth : 1,
//     order_num : 5,
//     sub_category : {
//       20 : {
//         category_no : 20,
//         parent_category_no : 5,
//         category_name : '공무원',
//         depth : 2,
//         order_num : 1
//       },
//       19 : {
//         category_no : 19,
//         parent_category_no : 5,
//         category_name : '자격증',
//         depth : 2,
//         order_num : 2
//       },
//       21 : {
//         category_no : 21,
//         parent_category_no : 5,
//         category_name : 'IT',
//         depth : 2,
//         order_num : 3
//       },
//       23 : {
//         category_no : 23,
//         parent_category_no : 5,
//         category_name : '외국어',
//         depth : 2,
//         order_num : 4
//       },
//       24 : {
//         category_no : 24,
//         parent_category_no : 5,
//         category_name : '인문',
//         depth : 5,
//         order_num : 2
//       },
//       22 : {
//         category_no : 22,
//         parent_category_no : 5,
//         category_name : '기타',
//         depth : 2,
//         order_num : 6
//       }
//     }
//   }
// };


const getCategoryObj = function(category_no){
  let category_obj;
  for(i in CATEGORY){
    if(category_no == CATEGORY[i].category_no){
      category_obj = CATEGORY[i];
      break;
    }
  }

  return category_obj;
};

const getSubCategoryObj = function(category_no, sub_category_no){
  let sub_category_obj;
  for(i in CATEGORY){
    if(category_no == CATEGORY[i].category_no){
      for(j in CATEGORY[i].sub_category){
        if(sub_category_no == CATEGORY[i].sub_category[j].category_no){
          sub_category_obj = CATEGORY[i].sub_category[j];
          break;
        }
      }
    }
  }

  return sub_category_obj;
};


const CATEGORY = [
  {
    category_no : 5,
    category_name : 'STUDY',
    depth : 1,
    order_num : 5,
    sub_category : [
      {
        category_no : 19,
        parent_category_no : 5,
        category_name : '자격증',
        depth : 2,
        order_num : 1
      },
      {
        category_no : 20,
        parent_category_no : 5,
        category_name : '공무원',
        depth : 2,
        order_num : 2
      },
      {
        category_no : 21,
        parent_category_no : 5,
        category_name : '외국어',
        depth : 2,
        order_num : 3
      },
      {
        category_no : 24,
        parent_category_no : 5,
        category_name : 'IT',
        depth : 5,
        order_num : 4
      },
      {
        category_no : 23,
        parent_category_no : 5,
        category_name : '인문지식',
        depth : 2,
        order_num : 5
      },
      {
        category_no : 22,
        parent_category_no : 5,
        category_name : '기타',
        depth : 2,
        order_num : 6
      }
    ]
  }
];



// const top_notice = [
//   {
//     date : '2019.06.03',
//     title : '거래소 상장',
//     desc : '네모코인 거래소 상장 소식을 알려드립니다.',
//     link : 'https://blog.naver.com/nemodax/221553218370'
//   },
//   {
//     date : '2019.06.03',
//     title : '파트너 모집',
//     desc : '특별한 인센티브 지급! 네모닥 파트너 CP를 모집합니다.',
//     link : 'https://blog.naver.com/nemodax/221553234659'
//   },
//   {
//     date : '2019.05.30',
//     title : '밋업 후기',
//     desc : '제 1회 네모데이 밋업에 참여해 주셔서 감사합니다.',
//     link : 'https://blog.naver.com/nemodax/221550537854'
//   }
// ];
