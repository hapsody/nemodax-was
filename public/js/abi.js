const abi = [
{
	"constant": true,
	"inputs": [],
	"name": "name",
	"outputs": [
		{
			"name": "",
			"type": "string"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_newAddr",
			"type": "address"
		}
	],
	"name": "upgrade",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_spender",
			"type": "address"
		},
		{
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "approve",
	"outputs": [
		{
			"name": "success",
			"type": "bool"
		}
	],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": true,
	"inputs": [],
	"name": "totalSupply",
	"outputs": [
		{
			"name": "",
			"type": "uint256"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_from",
			"type": "address"
		},
		{
			"name": "_to",
			"type": "address"
		},
		{
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "transferFrom",
	"outputs": [
		{
			"name": "success",
			"type": "bool"
		}
	],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": true,
	"inputs": [
		{
			"name": "",
			"type": "address"
		}
	],
	"name": "balances",
	"outputs": [
		{
			"name": "",
			"type": "uint256"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": true,
	"inputs": [],
	"name": "decimals",
	"outputs": [
		{
			"name": "",
			"type": "uint8"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": false,
	"inputs": [],
	"name": "unpause",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "burn",
	"outputs": [
		{
			"name": "success",
			"type": "bool"
		}
	],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_recipient",
			"type": "address"
		},
		{
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "withdrawEther",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": true,
	"inputs": [],
	"name": "runningAddress",
	"outputs": [
		{
			"name": "",
			"type": "address"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": true,
	"inputs": [
		{
			"name": "",
			"type": "address"
		},
		{
			"name": "",
			"type": "address"
		}
	],
	"name": "allowed",
	"outputs": [
		{
			"name": "",
			"type": "uint256"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": false,
	"inputs": [],
	"name": "exchangeEtherToToken",
	"outputs": [
		{
			"name": "success",
			"type": "bool"
		}
	],
	"payable": true,
	"stateMutability": "payable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "exchangeTokenToEther",
	"outputs": [
		{
			"name": "success",
			"type": "bool"
		}
	],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": true,
	"inputs": [
		{
			"name": "_account",
			"type": "address"
		}
	],
	"name": "balanceOf",
	"outputs": [
		{
			"name": "balance",
			"type": "uint256"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "target",
			"type": "address"
		}
	],
	"name": "unfreezeAccount",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_from",
			"type": "address"
		},
		{
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "burnFrom",
	"outputs": [
		{
			"name": "success",
			"type": "bool"
		}
	],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [],
	"name": "destroy",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [],
	"name": "pause",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": true,
	"inputs": [],
	"name": "symbol",
	"outputs": [
		{
			"name": "",
			"type": "string"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_recipient",
			"type": "address"
		},
		{
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "withdrawToken",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_to",
			"type": "address"
		},
		{
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "transfer",
	"outputs": [
		{
			"name": "success",
			"type": "bool"
		}
	],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_tokenName",
			"type": "string"
		},
		{
			"name": "_tokenSymbol",
			"type": "string"
		},
		{
			"name": "_initialSupply",
			"type": "uint256"
		},
		{
			"name": "_tokenPerEth",
			"type": "uint256"
		}
	],
	"name": "initExchanger",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": true,
	"inputs": [
		{
			"name": "",
			"type": "address"
		}
	],
	"name": "frozenAccount",
	"outputs": [
		{
			"name": "",
			"type": "bool"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_tokenPerEth",
			"type": "uint256"
		}
	],
	"name": "setExchangeRate",
	"outputs": [
		{
			"name": "success",
			"type": "bool"
		}
	],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": true,
	"inputs": [
		{
			"name": "_owner",
			"type": "address"
		},
		{
			"name": "_spender",
			"type": "address"
		}
	],
	"name": "allowance",
	"outputs": [
		{
			"name": "remaining",
			"type": "uint256"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "target",
			"type": "address"
		}
	],
	"name": "freezeAccount",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "newOwner",
			"type": "address"
		}
	],
	"name": "transferOwnership",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": false,
	"inputs": [
		{
			"name": "_recipient",
			"type": "address"
		}
	],
	"name": "destroyAndSend",
	"outputs": [],
	"payable": false,
	"stateMutability": "nonpayable",
	"type": "function"
},
{
	"constant": true,
	"inputs": [],
	"name": "getExchangerRate",
	"outputs": [
		{
			"name": "",
			"type": "uint256"
		}
	],
	"payable": false,
	"stateMutability": "view",
	"type": "function"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "from",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "etherValue",
			"type": "uint256"
		},
		{
			"indexed": false,
			"name": "tokenPerEth",
			"type": "uint256"
		}
	],
	"name": "ExchangeEtherToToken",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "from",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "etherValue",
			"type": "uint256"
		},
		{
			"indexed": false,
			"name": "tokenPerEth",
			"type": "uint256"
		}
	],
	"name": "ExchangeTokenToEther",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "to",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "value",
			"type": "uint256"
		}
	],
	"name": "WithdrawToken",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "to",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "value",
			"type": "uint256"
		}
	],
	"name": "WithdrawEther",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "from",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "tokenPerEth",
			"type": "uint256"
		}
	],
	"name": "SetExchangeRate",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "from",
			"type": "address"
		},
		{
			"indexed": true,
			"name": "to",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "value",
			"type": "uint256"
		}
	],
	"name": "Transfer",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "account",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "value",
			"type": "uint256"
		}
	],
	"name": "LastBalance",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "_owner",
			"type": "address"
		},
		{
			"indexed": true,
			"name": "_spender",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "_value",
			"type": "uint256"
		}
	],
	"name": "Approval",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "from",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "value",
			"type": "uint256"
		}
	],
	"name": "Burn",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": false,
			"name": "target",
			"type": "address"
		},
		{
			"indexed": false,
			"name": "frozen",
			"type": "bool"
		}
	],
	"name": "FrozenFunds",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [
		{
			"indexed": true,
			"name": "newContract",
			"type": "address"
		}
	],
	"name": "Upgraded",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [],
	"name": "Pause",
	"type": "event"
},
{
	"anonymous": false,
	"inputs": [],
	"name": "Unpause",
	"type": "event"
}
];
