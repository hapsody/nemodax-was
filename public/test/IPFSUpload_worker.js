
var window = {}
importScripts('./js/ipfs_index.js');
importScripts('./js/ipfs-http-client_index.js');
self.IpfsHttpClient = window.IpfsHttpClient
self.Ipfs = window.Ipfs
//console.log("window in worker:", window)
console.log("self in worker:", self);
// onmessage = function(event){
//   var AA = event.data; //event.data
//   AA.d = 44
//   var results = AA;
//
//   postMessage(results);
// }


 function createClient(context) {
  //var selectedClient = $('#ipfs_client').val();
  //console.log(context.selectedClient);
  context.ipfs = new Array(context.numOfChunksInThread)

  if (context.selectedClient == 'infura') {
    for (var i = 0; i < context.numOfChunksInThread; i++) {
      context.ipfs[i] = self.IpfsHttpClient({
        host: 'ipfs.infura.io',
        port: 5001,
        protocol: 'https'
      });
    }
    //$('#title').html('IPFS Client TEST (Infura)');
  }

}


async function sendIpfsWebWorker(context) {
  createClient(context)
  console.log("context in worker: ", context)
  // var uploadStartTime = new Date();
  // console.log("> start upload (api): ", uploadStartTime)

  //var path = $("#file").val().split("\\")[2]

  context.results = new Array(context.numOfChunksInThread)
  context.hashBuffer = new Array(context.numOfChunksInThread)
  context.uploadedChunkCnt = 0;


  for (var i = 0; i < context.numOfChunksInThread; i++) {
    var chunkStartTime = new Date();
    console.log("start upload chunk[", context.thread_no, "](api): ", chunkStartTime)

    context.ipfs[i].add([{
      path: context.path + '.nemo.' + context.thread_no,
      content: Ipfs.Buffer(context.uploadBuf.data),
    }], {
      recursive: true,
      wrapWithDirectory: true
      //progress: (length) => {
      //  console.log(length);
      //}
    }).then((res) => {
      var token = res[0].path.split('.')
      var chunkNum = token[token.length - 1]
      context.results[chunkNum] = res;

      console.log(context.results[chunkNum]);
      var chunkEndTime = new Date();
      var chunkInterval = chunkEndTime - chunkStartTime // ms
      var chunkSize = context.uploadBuf.data.length // byte
      var speed = chunkSize / chunkInterval // byte / ms == kb/s
      console.log('interval upload chunk[', chunkNum, '](api): ', chunkInterval / 1000, 'size:', chunkSize / 1000, 'kb', speed, 'kb/s')

      context.hashBuffer = context.results[chunkNum][1].hash;
      console.log(context.hashBuffer);
      context.uploadedChunkCnt++;

      var resultForMainThread = JSON.parse(JSON.stringify(context));
      postMessage(resultForMainThread);
      self.close()
      // if (uploadedChunkCnt == context.numOfChunksInThread) {
      //   var chunkMap = ""
      //   for (var j = 0; j < context.numOfChunksInThread; j++) {
      //     chunkMap = chunkMap + context.hashBuffer[j] + ','
      //   }
      //   context.ipfs[0].add(Ipfs.Buffer.from(chunkMap)).then((res) => {
      //     context.indexHash = res[0].hash
      //     var uploadFileSize = context.fullLength // byte
      //
      //     var resultForMainThread = JSON.parse(JSON.stringify(context));
      //     postMessage(resultForMainThread);
      //
      //     var uploadEndTime = new Date();
      //     var uploadInterval = uploadEndTime - uploadStartTime // ms
      //     var uploadSpeed = uploadFileSize / uploadInterval // byte / ms == kb/s
      //     console.log('* upload hash:', context.indexHash)
      //     console.log('* chunkMap:', chunkMap);
      //     console.log('* upload path:', context.path)
      //     console.log('* upload size:', formatSizeUnits(uploadFileSize))
      //     console.log('* upload speed:', uploadSpeed, 'kb/s')
      //     console.log('* upload interval:', uploadInterval / 1000, 'sec')
      //
      //
      //   });
      // }
    })
  }
}


function formatSizeUnits(bytes) {
  if (bytes >= 1073741824) {
    bytes = (bytes / 1073741824).toFixed(2) + " GB";
  } else if (bytes >= 1048576) {
    bytes = (bytes / 1048576).toFixed(2) + " MB";
  } else if (bytes >= 1024) {
    bytes = (bytes / 1024).toFixed(2) + " KB";
  } else if (bytes > 1) {
    bytes = bytes + " bytes";
  } else if (bytes == 1) {
    bytes = bytes + " byte";
  } else {
    bytes = "0 bytes";
  }
  return bytes;
}


onmessage = function(event){

  sendIpfsWebWorker(event.data);

}
