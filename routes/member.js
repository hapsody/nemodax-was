var express = require('express');
var url  = require('url');
var qs = require('querystring');
var http = require('http');
var https = require('https');
var sanitizeHtml = require('sanitize-html');
var router = express.Router();


/* GET login page. */
router.get('/login', function(req, res, next) {

  console.log(`/member/login`);

  res.render('member/login', { title: 'NemoDax Login', layout: 'layouts/layout-no' });

});


/* login porcess */
router.get('/login_process', function(req, res, next) {
  var params = url.parse(req.url, true).query;

  // HTTPRequest의 옵션 설정
  var options = {
    host: req.config.info.api_server_host,
    port: req.config.info.api_server_port,
    path: `/api/member/login.do?${qs.stringify(params)}`,
    method: 'GET'
  };

  // 콜백 함수로 Response를 받아온다
  var callback = function(response){
    // response 이벤트가 감지되면 데이터를 body에 받아온다
    var body = '';
    response.on('data', function(data) {
      body += data;
    });

    // end 이벤트가 감지되면 데이터 수신을 종료하고 내용을 출력한다
    response.on('end', function() {
      // 데이저 수신 완료
      console.log(body);
      var objResult = JSON.parse(body);

      if(objResult.result.code === 0){
        let options = {
          httpOnly: true, // The cookie only accessible by the web server
          path: '/',
          maxAge: 5000 * 60 * 60 // 쿠키 유효기간 2시간
        }
        res.cookie('NMT', objResult.data.token, options);
        res.cookie('n_user', objResult.data.mem_no, options);
      }

      res.send(objResult);
    });

    response.on('error', function(e){
      console.log(e);
    });
  }

  // 서버에 HTTP Request 를 날린다.
  var request;
  if(req.config.info.api_server_port === '443'){
    request = https.request(options, callback);
  }else{
    request = http.request(options, callback);
  }

  request.end();

});

/* login porcess */
router.get('/logout', function(req, res, next) {
  console.log(`/member/logout`);

  let cookie = req.cookies;
  for (var prop in cookie) {
      if (!cookie.hasOwnProperty(prop)) {
          continue;
      }
      res.cookie(prop, '', {expires: new Date(0)});
  }
  res.redirect('/');
});



/* GET signup page. */
router.get('/signup', function(req, res, next) {
  let params = url.parse(req.url, true).query;
  let email_yn = 'N';
  if(params.email_yn !== undefined){
    email_yn = params.email_yn;
  }
  let terms_yn = 'N';
  if(params.terms_yn !== undefined){
    terms_yn = params.terms_yn;
  }
  let privacy_yn = 'N';
  if(params.privacy_yn !== undefined){
    privacy_yn = params.privacy_yn;
  }

  res.render('member/signup', { title: 'NemoDax signup', email_yn: email_yn, terms_yn: terms_yn, privacy_yn: privacy_yn, layout: 'layouts/layout-no' });

});


/* POST signup process */
router.post('/signup_process', function(req, res, next) {
  console.log('/member/signup_process');
  console.log(req.body);

  var params = req.body;

  // HTTPRequest의 옵션 설정
  var options = {
    host: req.config.info.api_server_host,
    port: req.config.info.api_server_port,
    path: `/api/member/join.do`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': Buffer.byteLength(qs.stringify(params))
    }
  };

  // 콜백 함수로 Response를 받아온다
  var callback = function(response){
    // response 이벤트가 감지되면 데이터를 body에 받아온다
    var body = '';
    response.on('data', function(data) {
      body += data;
    });

    // end 이벤트가 감지되면 데이터 수신을 종료하고 내용을 출력한다
    response.on('end', function() {
      // 데이저 수신 완료
      console.log(body);
      var objResult = JSON.parse(body);

      if(objResult.result.code === 0){

      }

      res.send(objResult);
    });

    response.on('error', function(e){
      console.log(e);
    });
  }

  // 서버에 HTTP Request 를 날린다.
  var request;
  if(req.config.info.api_server_port === '443'){
    request = https.request(options, callback);
  }else{
    request = http.request(options, callback);
  }

  request.write(qs.stringify(params));
  request.end();

});



router.get('/terms', function(req, res, next) {
  res.render('member/terms', { title: 'NemoDax signup', layout: 'layouts/layout-no' });
});


router.get('/pw', function(req, res, next) {
  res.render('member/pw', { title: 'NemoDax signup', layout: 'layouts/layout-no' });
});

router.get('/pwreset', function(req, res, next) {
  res.render('member/pwreset', { title: 'NemoDax signup', layout: 'layouts/layout-no' });
});

module.exports = router;
