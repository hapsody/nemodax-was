var express = require('express');
var url  = require('url');
var qs = require('querystring');
var http = require('http');
var https = require('https')
var cookie = require('cookie');
var sanitizeHtml = require('sanitize-html');

var router = express.Router();

/* Get Api */
router.get('/getApi', function(req, res, next) {
  console.log(`/api/getApi :: ${req.config.info.id}`);
  var params = url.parse(req.url, true).query;
  var call_url = params.call_url; //api 서버에 호출하게 될 URL
  delete params.call_url;         //api 서버에 호출하게 될 URL 은 파라미터에서 제거.

  var headers = {
    'Content-Type' : 'application/x-www-form-urlencoded'
  };

  //쿠키값 존재 여부에 따라 토근 값 셋팅 및 call_url 변경.
  if(req.headers.cookie !== undefined && cookie.parse(req.headers.cookie).NMT !== undefined){
    var cookies = cookie.parse(req.headers.cookie);
    headers.Authorization = ` Bearer ${cookies.NMT}`;
  }else{
    //아래의 url 일 경우 로그인이 안되어 있으면 비로그인 url 로 교체하여 호출한다.
    if(call_url.indexOf('selectContentList.do') > -1){
      call_url = '/api/file/selectContentListNotLogin.do';
    }
  }

  // HTTPRequest의 옵션 설정
  var options = {
    headers: headers,
    host: req.config.info.api_server_host,
    port: req.config.info.api_server_port,
    path: `${call_url}?${qs.stringify(params)}`,
    method: 'GET'
  };

  // 콜백 함수로 Response를 받아온다
  var callback = function(response){
    // response 이벤트가 감지되면 데이터를 body에 받아온다
    var body = '';
    response.on('data', function(data) {
      body += data;
    });

    // end 이벤트가 감지되면 데이터 수신을 종료하고 내용을 출력한다
    response.on('end', function() {
      // 데이저 수신 완료
      //console.log(body);
      var objResult = JSON.parse(body);

      res.send(objResult);
    });

    response.on('error', function(e){
      console.log(e);
    });
  }

  // 서버에 HTTP Request 를 날린다.
  var request;
  if(req.config.info.api_server_port === '443'){
    request = https.request(options, callback);
  }else{
    request = http.request(options, callback);
  }

  request.end();

});



/* Post Api */
router.post('/postApi', function(req, res, next) {
  console.log('/api/postApi');

  var params = req.body;
  var call_url = params.call_url; //api 서버에 호출하게 될 URL
  delete params.call_url;         //api 서버에 호출하게 될 URL 은 파라미터에서 제거.

  for(i in params){
    params[i] = sanitizeHtml(params[i], {
      allowedTags: ['a'],
      allowedAttributes: {
        'a': [ 'href' ]
      }
    });
  }


  var headers = {
    'Content-Type' : 'application/x-www-form-urlencoded',
    'Content-Length': Buffer.byteLength(qs.stringify(params))
  };

  //쿠키값 존재 여부에 따라 토근 값 셋팅 및 call_url 변경.
  if(req.headers.cookie !== undefined && cookie.parse(req.headers.cookie).NMT !== undefined){
    var cookies = cookie.parse(req.headers.cookie);
    headers.Authorization = ` Bearer ${cookies.NMT}`;
  }else{
    //아래의 url 일 경우 로그인이 안되어 있으면 비로그인 url 로 교체하여 호출한다.

  }

  // HTTPRequest의 옵션 설정
  var options = {
    host: req.config.info.api_server_host,
    port: req.config.info.api_server_port,
    path: `${call_url}`,
    method: 'POST',
    headers: headers
  };

  // 콜백 함수로 Response를 받아온다
  var callback = function(response){
    // response 이벤트가 감지되면 데이터를 body에 받아온다
    var body = '';
    response.on('data', function(data) {
      body += data;
    });

    // end 이벤트가 감지되면 데이터 수신을 종료하고 내용을 출력한다
    response.on('end', function() {
      // 데이저 수신 완료
      //console.log(body);
      var objResult = JSON.parse(body);

      res.send(objResult);
    });

    response.on('error', function(e){
      console.log(e);
    });
  }

  // 서버에 HTTP Request 를 날린다.
  var request;
  if(req.config.info.api_server_port === '443'){
    request = https.request(options, callback);
  }else{
    request = http.request(options, callback);
  }

  request.write(qs.stringify(params));
  request.end();

});

module.exports = router;
