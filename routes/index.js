var express = require('express');
var http = require('http');
var qs = require('querystring');
var url  = require('url');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('index', { title: 'NemoDax' });
});



/*  */
router.get('/file/create', function(req, res, next) {
  res.render('file/create', { title: 'NemoDax' });
});

router.get('/file/create_new', function(req, res, next) {
  res.render('file/create_new', { title: 'NemoDax' });
});

router.get('/file/create_series', function(req, res, next) {
  res.render('file/create_series', { title: 'NemoDax' });
});

router.get('/wallet', function(req, res, next) {
  res.render('wallet_test', { title: 'NemoDax' });
});

router.get('/charge', function(req, res, next) {
  res.render('charge_test', { title: 'NemoDax' });
});

router.get('/settings', function(req,res, next){
  res.render('settings/settings', { title: 'Nemodax' });
});

router.get('/history', function(req,res, next){
  res.render('settings/history', { title: 'Nemodax' });
});

router.get('/transactions', function(req,res, next){
  res.render('settings/transactions', { title: 'Nemodax' });
});

router.get('/salesHistory', function(req,res, next){
  res.render('settings/salesHistory', { title: 'Nemodax' });
});

router.get('/myData', function(req,res, next){
  res.render('settings/myData', { title: 'Nemodax' });
});

router.get('/guide/roadmap', function(req,res, next){
  res.render('guide/roadmap', { title: 'Nemodax' });
});

router.get('/service/*', function(req,res, next){
  let last_path = req.path.split('/')[2];

  if(last_path === 'ieo' || last_path === 'ieo_en'){
    res.render(`service/${last_path}`, { title: 'Nemodax', layout: 'layouts/layout-no'});
  }else{
    res.render(`service/${last_path}`, { title: 'Nemodax'});
  }

});

module.exports = router;
